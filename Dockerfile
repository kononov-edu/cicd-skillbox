FROM python:3-slim

WORKDIR /code
COPY . /code/
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 80
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:80
